package com.zuitt.batch193;

import java.util.InputMismatchException;
import java.util.Scanner;

public class WDC043_S3_A1 {

    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);
//        System.out.println("Input an integer whose factorial will be computed");
        int in = 0;
//        long factorial = 1;
//        for( int i = 1; i <= in; i++) {
//            factorial *= i;
//        }
//        System.out.println("Factorial of " + in + " is: " + factorial);

        try {
            System.out.println("Input an integer whose factorial will be computed");
            in = input.nextInt();
        } catch (InputMismatchException e) {
            System.out.println("Entered Input is not a number");
        } finally {
            if (in != 0) {
                long factorial = 1;
                for (int i = 1; i <= in; i++) {
                    factorial *= i;
                } System.out.println("Factorial of " + in + " is: " + factorial);
            }
        }
    }
}